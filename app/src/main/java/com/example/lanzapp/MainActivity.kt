package com.example.lanzapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import java.util.*

class MainActivity : AppCompatActivity() {
    //inicializacion tardi
    lateinit var imageDice1 : ImageView
    lateinit var imageDice2 : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollBtn : Button = findViewById(R.id.btn_dice)

        imageDice1 = findViewById(R.id.img_dice1)
        imageDice2 = findViewById(R.id.img_dice2)

        rollBtn.setOnClickListener{
            roll()
        }
    }

    fun roll(){
        tombola(imageDice1)
        tombola(imageDice2)

        //debug
        Log.d("Lanzapp","click!")
    }

    fun tombola(imageDice : ImageView) {

        val randomInt = Random().nextInt(6) + 1
        val drawableResource = when(randomInt){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_6
            else -> R.drawable.dice_6
        }
        imageDice.setImageResource(drawableResource)
    }

}
